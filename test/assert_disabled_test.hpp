#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, Assert )
{
	EXPECT_NO_THROW ( {
		ASSERT( true_assertion, true );
	} );

	EXPECT_NO_THROW ( {
		ASSERT( false_assertion, false );
	} );
}
