#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, LoopVariant )
{
	// variant is not evaluated when created
	EXPECT_NO_THROW ( {
		CREATE_LOOP_VARIANT( n_is_positive, 0 );
	} );

	// on first iteration variant holds
	EXPECT_NO_THROW ( {
		CREATE_LOOP_VARIANT( n_is_positive, 1 );
		UPDATE_LOOP_VARIANT( n_is_positive );
	} );

	// on second iteration the variant is not changed - violation!
	EXPECT_NO_THROW ( {
		CREATE_LOOP_VARIANT( n_is_positive, 1 );
		UPDATE_LOOP_VARIANT( n_is_positive );
		// but as checks are disabled we don't throw
		UPDATE_LOOP_VARIANT( n_is_positive );
	} );

	// on first iteration the variant is exhausted - violation!
	EXPECT_NO_THROW ( {
		CREATE_LOOP_VARIANT( n_is_positive, 0 );
		// but as checks are disabled we don't throw
		UPDATE_LOOP_VARIANT( n_is_positive );
	} );

	// variant holds on both iterations
	EXPECT_NO_THROW ( {
		int n = 2;
		CREATE_LOOP_VARIANT( n_is_positive, n );
		UPDATE_LOOP_VARIANT( n_is_positive );
		--n;
		UPDATE_LOOP_VARIANT( n_is_positive );
	} );

	// variant is exhausted on second iteration - violation!
	EXPECT_NO_THROW ( {
		int n = 1;
		CREATE_LOOP_VARIANT( n_is_positive, n );
		UPDATE_LOOP_VARIANT( n_is_positive );
		--n;
		// but as checks are disabled we don't throw
		UPDATE_LOOP_VARIANT( n_is_positive );
	} );
}

//--------------------------------------------------------------------------------
