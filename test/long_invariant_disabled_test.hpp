#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, LongInvariant )
{
	EXPECT_NO_THROW ( {
		LONG_INVARIANT( true_invariant, true );
	} );

	EXPECT_NO_THROW ( {
		LONG_INVARIANT( false_invariant, false );
	} );
}

//--------------------------------------------------------------------------------
