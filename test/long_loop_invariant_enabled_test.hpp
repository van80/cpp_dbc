#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, LongLoopInvariant )
{
	EXPECT_NO_THROW ( {
		LONG_LOOP_INVARIANT( true_loop_invariant, true );
	} );

	EXPECT_THROW ( {
		LONG_LOOP_INVARIANT( false_loop_invariant, false );
	}, dbc::loop_invariant_violated );
}

//--------------------------------------------------------------------------------
