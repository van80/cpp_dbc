#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, LongPostcondition )
{
	EXPECT_NO_THROW ( {
		LONG_POSTCONDITION( true_postcondition, true );
	} );

	EXPECT_THROW ( {
		LONG_POSTCONDITION( false_postcondition, false );
	}, dbc::postcondition_violated );
}

//--------------------------------------------------------------------------------
