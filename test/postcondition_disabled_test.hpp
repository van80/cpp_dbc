#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, Postcondition )
{
	EXPECT_NO_THROW ( {
		POSTCONDITION( true_postcondition, true );
	} );

	EXPECT_NO_THROW ( {
		POSTCONDITION( false_postcondition, false );
	} );
}

//--------------------------------------------------------------------------------
