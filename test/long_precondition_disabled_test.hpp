#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, LongPrecondition )
{
	EXPECT_NO_THROW ( {
		LONG_PRECONDITION( true_precondition, true );
	} );

	EXPECT_NO_THROW ( {
		LONG_PRECONDITION( false_precondition, false );
	} );
}

//--------------------------------------------------------------------------------
