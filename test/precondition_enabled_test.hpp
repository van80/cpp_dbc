#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, Precondition )
{
	EXPECT_NO_THROW ( {
		PRECONDITION( true_precondition, true );
	} );

	EXPECT_THROW ( {
		PRECONDITION( false_precondition, false );
	}, dbc::precondition_violated );
}

//--------------------------------------------------------------------------------
