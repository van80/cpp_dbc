#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, LoopInvariant )
{
	EXPECT_NO_THROW ( {
		LOOP_INVARIANT( true_loop_invariant, true );
	} );

	EXPECT_NO_THROW ( {
		LOOP_INVARIANT( false_loop_invariant, false );
	} );
}

//--------------------------------------------------------------------------------
