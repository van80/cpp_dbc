#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, LongAssert )
{
	EXPECT_NO_THROW ( {
		LONG_ASSERT( true_assertion, true );
	} );

	EXPECT_THROW ( {
		LONG_ASSERT( false_assertion, false );
	}, dbc::assertion_failed );
}

//--------------------------------------------------------------------------------
