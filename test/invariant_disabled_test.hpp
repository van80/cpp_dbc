#pragma once

//--------------------------------------------------------------------------------

TEST( Dbc, Invariant )
{
	EXPECT_NO_THROW ( {
		INVARIANT( true_invariant, true );
	} );

	EXPECT_NO_THROW ( {
		INVARIANT( false_invariant, false );
	} );
}

//--------------------------------------------------------------------------------
