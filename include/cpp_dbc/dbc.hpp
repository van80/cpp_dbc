#pragma once
/*! \file dbc.hpp
 * \brief Macros facilitating use of Design By Contract in C++ code
 */

#include <exception>
#include <functional>


#ifdef CPP_DBC_ALL_CHECKS

	#define CPP_DBC_ENABLE_PRECONDITION_CHECK
	#define CPP_DBC_ENABLE_POSTCONDITION_CHECK
	#define CPP_DBC_ENABLE_INVARIANT_CHECK
	#define CPP_DBC_ENABLE_ASSERTIONS
	#define CPP_DBC_ENABLE_LOOP_VARIANT_CHECK
	#define CPP_DBC_ENABLE_LOOP_INVARIANT_CHECK
	#define CPP_DBC_ENABLE_LONG_CHECK

#elif defined CPP_DBC_NO_LONG_CHECKS

	#define CPP_DBC_ENABLE_PRECONDITION_CHECK
	#define CPP_DBC_ENABLE_POSTCONDITION_CHECK
	#define CPP_DBC_ENABLE_INVARIANT_CHECK
	#define CPP_DBC_ENABLE_ASSERTIONS
	#define CPP_DBC_ENABLE_LOOP_VARIANT_CHECK
	#define CPP_DBC_ENABLE_LOOP_INVARIANT_CHECK

#elif defined CPP_DBC_MINIMUM_CHECKS

	#define CPP_DBC_ENABLE_PRECONDITION_CHECK

#elif CPP_DBC_NO_CHECKS

	// Nothing to do

#else

	#error "No DBC check level is provided"

#endif

namespace dbc {

//================================================================================

//! Exception thrown when precondition of a function is violated
/*!
 * This exception means that client of a function has provided an argument that does not belong to the function's domain,
 * i.e. there is a bug on the client's side.
 * \anchor precondition_violated
 */
typedef ::std::domain_error precondition_violated;

//================================================================================

//! Exception thrown when postondition of a function is violated
/*!
 * This exception means that due to a bug in implementation or due to some external factors
 * a function failed to delivered result promised by it in its contract.
 * \anchor postcondition_violated
 */
typedef ::std::runtime_error postcondition_violated;

//================================================================================

//! Exception thrown when invariant of type gets violated.
/*!
 * This exception means that due to some error an instance of a class has come to invalid state.
 * \anchor invariant_violated
 */
typedef ::std::runtime_error invariant_violated;

//================================================================================

//! Exception thrown when loop variant is violated
/*!
 * This exception means that a loop does not terminate.
 * \anchor loop_variant_violated
 */
typedef ::std::runtime_error loop_variant_violated;

//================================================================================

//! Exception thrown when loop invariant is violated
/*!
 * This exception means that result of algorithm execution will be incorrect.
 * \anchor loop_invariant_violated
 */
typedef ::std::runtime_error loop_invariant_violated;

//================================================================================

//! Exception thrown when some intermediate assertion fails.
/*!
 * This assertion is an indication that the code throwing it has some bug in its implementation.
 * \anchor assertion_failed
 */
typedef ::std::runtime_error assertion_failed;

//--------------------------------------------------------------------------------

//! \cond HIDDEN_SYMBOLS
// This class is an implementation detail and it should not be used directly in your code
template <class T>
class dbc_loop_variant
{
public:

	dbc_loop_variant( const char* name, const char* expr, std::function< T() > func ) 
		: m_name( name )
		, m_expr( expr )
		, m_func( func )
		, m_old_value( T() ) 
	{}

	// loop variant is a POSITIVE number which gradually falls down to 0.
	// Reaching 0 means that the loop must have already been aborted - this situation should never occur.
	void update_loop_variant( const char* calling_function, const char* file_name, int line_number )
	{
		static const T min_value = T();
		T new_value = m_func();

		/*
		Documentation notation:
		v - is a decreasing (starting from element #1) sequence of natural numbers called loop variant,
		v(0) = 0 - value of loop variant before the 1st iteration;
		v(k) - value of loop variant at k-th iteration;

		Given a system of inequalities which holds on each iteration k=1,2,3,... of some algorithm

			0 < v(k)							(1.a)
			v(k) < v(k-1) || v(k-1) = 0,		(1.b)

		where v(0) is defined as

			v(0) = 0							(2)

		then the maximum amount of iterations this algorithm can have is v(1).

		Indeed if v(k) = 0 then (1.a) is false on the very first iteration
		and therefore no iteration is possible with (1.a) being true.

		If 0 < v(1) then at the first iteration (k=1) the inequality (1.a) holds trivially
		and the inequality (1.b) holds because v(k-1) = v(0) = 0 by definition (2).
		Therefore we know that for 0 < v(1) the inequlities (1) hold on the first iteration.

		The inequality v(k) < v(k-1), the left operand of (1.b), and the inequality (1.a) 
		can hold simultaneously only at most v(1)-1 following iterations: 

			2, 3, ..., v(1)

		and then value v(k) will have to reach 0 because v(1) is finite 
		and there are only v(1) distinct numbers such that 0 < v(k) <= v(1).
		That is on iteration v(1)+1 as the latest either 0 < v(k) or v(k) < v(k-1) will turn false.

		The system of inequalities (1) can be used to verify the claim 
		that some algorithm terminates within "m" iterations maximum.
		The loop variant at the first iteration can be assigned to be v(1) >= m.
		If v decreases sufficiently slow i.e. by 1 at each iteration
		then the loop will terminate before (1.a) turns false.

		This can be expressed as a predicate:

		P1: Loop terminates in at most m steps => Loop variant holds on each iteration

		Which gives us by contrapositive rule the following statement

		P2: Loop variant DOES NOT hold on some iteration => Loop DOES NOT terminate in at most m steps.

		Therefore we can use the loop variant inequality system (1)
		as an indicator (when it fails to hold) that loop has not terminated while it had to.

		On practice it is more convenient to give v(1) a bigger value then expected m
		because the problem we usually need to deal with is non-terminating loops - the situation
		when any finite upper bound is sufficient.
		*/

		if ( (min_value < new_value) && ( (new_value < m_old_value) || !(min_value < m_old_value) ) )
		{
			m_old_value = new_value;
		} // if variant is not exhausted
		else
		{
			throw loop_variant_violated (
				std::string("[") 
				+ calling_function + "], " 
				+ "Loop Variant " 
				+ m_name 
				+ " violated: " 
				+ m_expr 
				+ ".\n\t"
				+ "old_value=" + std::to_string( m_old_value )
				+ ", new_value=" + std::to_string( new_value )
				+ "\n\t" 
				+ "at " + file_name + ":" 
				+ std::to_string( line_number )
			);
		} // else
	} // update_loop_variant

private:

	const char* m_name;
	const char* m_expr;
	std::function< T() > m_func;
	T m_old_value;
};
//! \endcond

//================================================================================

} // namespace dbc



//--------------------------------------------------------------------------------

//! Check a precondition and throw \ref precondition_violated exception if the precondition is violated.
/*!
 * \param name - name of precondition, used for better readability of exceptions;
 * \param expr - expression evaluating to true if precondition holds or false if it does not hold.
 * 
 * \par Requirement to \p expr: 
 * Algorithmic complexity of \p expr must be not greater than complexity of the function in question.
 */
#ifdef CPP_DBC_ENABLE_PRECONDITION_CHECK
	#define PRECONDITION( name, expr ) if ( !(expr) ) throw ::dbc::precondition_violated( \
				std::string("[") \
				+ __FUNCTION__ + "], " \
				+ "Precondition " \
				+ #name \
				+ " violated: " \
				+ #expr \
				+ "\n\t" \
				+ "at " __FILE__ + ":" \
				+ std::to_string(__LINE__) \
			);
#else 
	#define PRECONDITION( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check a precondition and throw \ref precondition_violated exception if the precondition is violated.
/*!
 * \param name - name of precondition, used for better readability of exceptions;
 * \param expr - expression evaluating to true if precondition holds or false if it does not hold.
 *
 * This assertion can be used with expressions having higher algorithmic complexity than the function in question.
 * Use of this assertion allows disabling it selectively in release build.
 */
#ifdef CPP_DBC_ENABLE_LONG_CHECK
	#define LONG_PRECONDITION( name, expr ) PRECONDITION( name, expr )
#else 
	#define LONG_PRECONDITION( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check a postcondition and throw \ref postcondition_violated exception if the postcondition is violated.
/*!
 * \param name - name of postcondition, used for better readability of exceptions;
 * \param expr - expression evaluating to true if postcondition holds or false if it does not hold.
 *
 * \par Requirement to \p expr: 
 * Algorithmic complexity of \p expr must be not greater than complexity of the function in question.
 */
#ifdef CPP_DBC_ENABLE_POSTCONDITION_CHECK
	#define POSTCONDITION( name, expr ) if ( !(expr) ) throw ::dbc::postcondition_violated( \
				std::string("[") \
				+ __FUNCTION__ + "], " \
				+ "Postcondition " \
				+ #name \
				+ " failed: " \
				+ #expr \
				+ "\n\t" \
				+ "at " __FILE__ + ":" \
				+ std::to_string(__LINE__) \
			);
#else 
	#define POSTCONDITION( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check a postcondition and throw \ref postcondition_violated exception if the postcondition is violated.
/*!
 * \param name - name of postcondition, used for better readability of exceptions;
 * \param expr - expression evaluating to true if postcondition holds or false if it does not hold.
 *
 * This assertion can be used with expressions having higher algorithmic complexity than the function in question.
 * Use of this assertion allows disabling it selectively in release build.
 */
#ifdef CPP_DBC_ENABLE_LONG_CHECK
	#define LONG_POSTCONDITION( name, expr ) POSTCONDITION( name, expr )
#else 
	#define LONG_POSTCONDITION( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check a type invariant and throw \ref invariant_violated exception if the invariant is violated.
/*!
 * \param name - name of an invariant property being checked, used for better readability of exceptions;
 * \param expr - expression evaluating to true if invariant property holds or false if it does not hold.
 *
 * \par Requirement to \p expr: 
 * Algorithmic complexity of \p expr must be not greater than complexity of the function where it is called.
 * This macro is unlikely to be used in the code but if it is used 
 * then it can be called only in the beginning or in the end of function (usually class method).
 */
#ifdef CPP_DBC_ENABLE_INVARIANT_CHECK
	#define INVARIANT( name, expr ) if ( !(expr) ) throw ::dbc::invariant_violated( \
				std::string("[") \
				+ __FUNCTION__ + "], " \
				+ "Postcondition " \
				+ #name \
				+ " failed: " \
				+ #expr \
				+ "\n\t" \
				+ "at " __FILE__ + ":" \
				+ std::to_string(__LINE__) \
			);
#else 
	#define INVARIANT( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check a type invariant and throw \ref invariant_violated exception if the invariant is violated.
/*!
 * \param name - name of an invariant property being checked, used for better readability of exceptions;
 * \param expr - expression evaluating to true if invariant property holds or false if it does not hold.
 *
 * \par Requirement to \p expr: 
 * This assertion can be used even with expressions having higher algorithmic complexity than the function in question.
 * This macro is unlikely to be used in the code but if it is used 
 * then it can be called only in the beginning or in the end of function (usually class method).
 * Use of this assertion allows disabling it selectively in release build.
 */
#ifdef CPP_DBC_ENABLE_LONG_CHECK
	#define LONG_INVARIANT( name, expr ) INVARIANT( name, expr )
#else 
	#define LONG_INVARIANT( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check an assertion and throw \ref assertion_failed exception if the assertion is failed.
/*!
 * \param name - name of the assertion, used for better readability of exceptions;
 * \param expr - expression evaluating to true if assertion holds or false if it does not hold.
 *
 * \par Requirement to \p expr: 
 * Evaluation of \p expr should not asympthoticly increase algorithmic complexity of the code in question.
 */
#ifdef CPP_DBC_ENABLE_ASSERTIONS
#define ASSERT( name, expr ) if ( !(expr) ) throw ::dbc::assertion_failed( \
			std::string("[") \
			+ __FUNCTION__ + "], " \
			+ "Assertion " \
			+ #name \
			+ " failed: " \
			+ #expr \
			+ "\n\t" \
			+ "at " __FILE__ + ":" \
			+ std::to_string(__LINE__) \
		);
#else 
	#define ASSERT( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check an assertion and throw \ref assertion_failed exception if the assertion is failed.
/*!
 * \param name - name of the assertion, used for better readability of exceptions;
 * \param expr - expression evaluating to true if assertion holds or false if it does not hold.
 *
 * This assertion can be used with expressions 
 * which can asympthotically increase algorithmic complexity of the code in question.
 * Use of this assertion allows disabling it selectively in release build.
 */
#ifdef CPP_DBC_ENABLE_LONG_CHECK
	#define LONG_ASSERT( name, expr ) ASSERT( name, expr )
#else 
	#define LONG_ASSERT( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Create loop variant before loop execution.
/*!
 * \param name - name of loop variant for future references in \ref UPDATE_LOOP_VARIANT;
 * \param expr - integral expression which value must decrease with each iteration.
 *
 * This expression is NOT evaluated while loop variant is created,
 * it will be only evaluated in the loop iterations.
 */
#ifdef CPP_DBC_ENABLE_LOOP_VARIANT_CHECK
	#define CREATE_LOOP_VARIANT( name, expr ) ::dbc::dbc_loop_variant< decltype(expr) > name( #name, #expr, [&](){ return (expr); } )
#else 
	#define CREATE_LOOP_VARIANT( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Update loop variant and throw \ref loop_variant_violated exception if its value has not decreased or if it has reached 0. 
/*!
 * \param name - name of loop variant created by \ref CREATE_LOOP_VARIANT.
 *
 * This macro evaluates \p expr from \ref CREATE_LOOP_VARIANT 
 * and compares it with the value obtained at the previous iteration if there were any iterations before.
 * If the value does not decrease it is an indication that the loop will not terminate
 * and therefoere \ref loop_variant_violated exception is thrown immediately.
 * In case if value of \p expr falls to 0 or below it this is an indication 
 * that loop has not terminated while it had to.
 * This situation also leads to emission of \ref loop_variant_violated exception.
 */
#ifdef CPP_DBC_ENABLE_LOOP_VARIANT_CHECK
	#define UPDATE_LOOP_VARIANT( name ) name.update_loop_variant( __FUNCTION__, __FILE__, __LINE__ );
#else 
	#define UPDATE_LOOP_VARIANT( name )
#endif

//--------------------------------------------------------------------------------

//! Check loop invariant and throw \ref loop_invariant_violated exception if it is violated.
/*!
 * \param name - name of loop invariant property, used for better readability of exceptions;
 * \param expr - expression evaluating to true if invariant property holds or false if it does not hold.
 * 
 * \par Requirement to \p expr: 
 * This macro should be used only with expressions which do not increase algorithmic complexity of the code in question.
 */
#ifdef CPP_DBC_ENABLE_LOOP_INVARIANT_CHECK
	#define LOOP_INVARIANT( name, expr ) if ( !(expr) ) throw ::dbc::loop_invariant_violated( \
				std::string("[") \
				+ __FUNCTION__ + "], " \
				+ "Loop Invariant " \
				+ #name \
				+ " failed: " \
				+ #expr \
				+ "\n\t" \
				+ "at " __FILE__ + ":" \
				+ std::to_string(__LINE__) \
			);
#else 
	#define LOOP_INVARIANT( name, expr )
#endif

//--------------------------------------------------------------------------------

//! Check loop invariant and throw \ref loop_invariant_violated exception if it is violated.
/*!
 * \param name - name of loop invariant property, used for better readability of exceptions;
 * \param expr - expression evaluating to true if invariant property holds or false if it does not hold.
 *
 * This macro can be used with expressions which can increase algorithmic complexity of the code in question.
 * Use of this assertion allows disabling it selectively in release build.
 */
#ifdef CPP_DBC_ENABLE_LONG_CHECK
	#define LONG_LOOP_INVARIANT( name, expr ) LOOP_INVARIANT( name, expr )
#else 
	#define LONG_LOOP_INVARIANT( name, expr )
#endif
