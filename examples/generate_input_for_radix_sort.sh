#!/usr/bin/env bash

this_script_name="$0"

function show_usage {
	echo "Usage: $this_script_name amount-of-dates" >&2
}

dates_count="$1"

if [ -z "$dates_count" ]; then
	show_usage
	exit 1
fi

for k in $(seq 1 "$dates_count"); do
	echo "$(($RANDOM % 10000))-$((1 + $RANDOM % 12))-$((1 + $RANDOM % 31))"
done
