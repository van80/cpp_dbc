#include <iostream>
#include <string>

#include <cpp_dbc/dbc.hpp>

typedef size_t number;

//--------------------------------------------------------------------------------

// Depending on typedef of number we will use appropriate specialization of to_number
template< class T > T to_number( const std::string& s );

//--------------------------------------------------------------------------------

template<> int to_number< int >( const std::string& s ) { return std::stoi( s ); }

//--------------------------------------------------------------------------------

template<> long to_number< long >( const std::string& s ) { return std::stol( s ); }

//--------------------------------------------------------------------------------

template<> long long to_number< long long >( const std::string& s ) { return std::stoll( s ); }

//--------------------------------------------------------------------------------

template<> unsigned int to_number< unsigned int >( const std::string& s ) { return static_cast< unsigned int >( std::stoul( s ) ); }

//--------------------------------------------------------------------------------

template<> unsigned long to_number< unsigned long >( const std::string& s ) { return std::stoul( s ); }

//--------------------------------------------------------------------------------

template<> unsigned long long to_number< unsigned long long >( const std::string& s ) { return std::stoull( s ); }

//--------------------------------------------------------------------------------

//! Integral logarithm of n base 2
/*!
 * \pre n > 0
 * \post $2^{\log_2 n} \leqslant n < 2^{\log_2 (n+1)}$
 */
number log2( const number n )
{
	PRECONDITION( n_is_positive, n > 0 );

	number k = 0; // iteration number
	number m = n; // value of m(k) after k-th iteration, see the meaning of m(k) below

	// Loop invariant:
	// ===============
	// 1. m(k) = floor( n / 2^k )
	// 2. 2^{k-1} <= n
	//
	// Base case:
	// ----------
	// n > 0 by precondition n_is_positive, k = 0
	// 1. m(0) = floor( n / 2^0 ) = floor( n / 1 ) = n
	// 2. 2^{-1} = 1/2 < 1 <= n
	//
	// Induction step:
	// ---------------
	// Given both properties hold after k-th iteration
	// we need to show that they will also hold after iteration k+1 
	// if such iteration ever occurs.
	//
	// #1. m(k+1) = (m(k) >> 2) 
	// 			= floor( m(k) / 2 ) 
	// 			= floor( floor( n / 2^k ) / 2 ), by invariant property #1
	// 	Let n = q * 2^k + r, 0 <= r < 2^k,
	// 	then 
	// 	m(k+1) = floor( floor( (q * 2^k + r) / 2^k ) / 2 ) 
	// 			= floor( floor( q + r / 2^k ) / 2 ) 
	// 			= floor( q / 2 + floor( r / 2^k ) / 2 ) 
	// 			= floor( q / 2 ), because r < 2^k.
	// 	Let's check that the value calculated by formula in p.1 is the same.
	// 	m(k+1) = floor( n / 2^(k+1) )
	// 		= floor( (q * 2^k + r) / 2^(k+1) )
	// 		= floor( q / 2 + r / 2^(k+1) )
	//
	// 	Consider two cases
	//
	// 	Case #1, q = 2l:
	// 	----------------
	// 	m(k+1) = floor( 2l / 2 + r / 2^(k+1) )
	// 		= l + floor( r / 2^(k+1) )
	// 		= l, because r < 2^k
	// 		= floor( q / 2 )
	//
	//	Case #2, q = 2l + 1:
	//	--------------------
	//	m' = floor( (2l + 1) / 2 + r / 2^(k+1) )
	//		= floor( l + 1 / 2 + r / 2^(k+1) )
	//		= l + floor( (2^k + r) / 2^(k+1) )
	//		= l, because r < 2^k and thereofore 2^k + r < 2^(k+1)
	//		= floor( q / 2 )
	//
	//	Therefore m(k+1) = floor( q / 2 ) = m(k) >> 2 is indeed floor(n / 2^(k+1))
	//
	// #2. Iteration k+1 occurs only if m(k) > 0, 
	// 	i.e. only if the value of m after k-th iteration is positive
	// 	which means that 2^(k+1-1) = 2^k <= n (by p.1)
	// 	This proves property #2.
	//
	// Termination:
	// ------------
	// m(k) = 0  =>  n < 2^k, i.e. value of m after k-th iteration is 0.
	// By p.2 2^{k-1} <= n
	// Taking logarithm base 2 in both inequalities we get: k-1 <= log2(n) < k
	// which means that floor of log2(n) is k-1 - the integral logarithm of n base 2

	CREATE_LOOP_VARIANT( m_decreases, m );
	
	while ( m )
	{
		UPDATE_LOOP_VARIANT( m_decreases );

		m >>= 1;
		++k;

		LOOP_INVARIANT( property_1, ((n >> (k-1)) >> 1) == m ); // floor( n / 2^k ) == m
		LOOP_INVARIANT( property_2, (1 << (k-1)) <= n ); // 2^{k-1} <= n
	}

	number result = k - 1;

	// We expect 2^{k-1} <= n < 2^k
	// These nice bounds cannot be used in postconditions though 
	// because 2^k can lead to overflow.
	// Therefore we have to rewrite this inequality as two equivalent forms:
	// 1 <= n / 2^{k-1} < 2 (a)
	// 1 / 2 <= n / 2^k < 1 (b)
	// Taking floor from (a) we get: 1 = (n >> (k-1)) < 2 - this is lower_bound
	// Taking floor from (b) we get: 0 = (n >> k) < 1 - this is upper_bound
	POSTCONDITION( lower_bound, 1 == (n >> result) ); // floor( n / 2^{k-1} ) 
	// The expression below does not exactly match the expression for upper_bound written above.
	// Nevertheless it is proven in the loop invariant secion that both expressions are equivalent.
	// The reason why we use this trick is that in boundary cases when n is close to max_int
	// shift on whole length of integer is simply ignored by the compiler or the processor, 
	// i.e. if we do (n >> (result+1)) then instead of expected 0 we get n.
	POSTCONDITION( upper_bound, 0 == ((n >> result) >> 1) );
	
	return result;
}

//--------------------------------------------------------------------------------

int main( int argc, char* argv[] )
{
	if ( 1 == argc )
	{
		std::cerr << "Usage: " << argv[ 0 ] <<  " positive-integer" << std::endl;
		return 1;
	}

	number x = to_number< number >( argv[ 1 ] );

	std::cout << log2( x ) << std::endl;

	return 0;
}

//--------------------------------------------------------------------------------
