// This program sorts array of dates in ascending order in linear time
// On its example you can get some feeling how DBC is supposed to be used

#include <cpp_dbc/dbc.hpp>

#include <iostream>
#include <algorithm> // minmax
#include <numeric> // algorithm
#include <utility> // std::pair
#include <vector>
#include <iterator>
#include <cmath>

//--------------------------------------------------------------------------------

struct date
{
	//! Constructs an impossible date
	date()
	: year( 0 )
	, month( 0 )
	, day_of_month( 0 )
	{
	}

	date( int year_arg, int month_arg, int day_of_month_arg )
	: year( year_arg )
	, month( month_arg )
	, day_of_month( day_of_month_arg )
	{
	}

	int year;
	int month;
	int day_of_month;
};

//--------------------------------------------------------------------------------

inline bool read_num_and_separator( std::istream& input, int& num, char expected_separator )
{
	char sep;
	input >> num >> sep;

	return expected_separator == sep;
}

//--------------------------------------------------------------------------------

inline std::istream& operator>>( std::istream& input, date& dt )
{
	int y, m, d;
	char sep;
	const static char expected_sep='-';

	if ( read_num_and_separator( input, y, expected_sep )
		&& read_num_and_separator( input, m, expected_sep )
		&& (input >> d) )
	{
		dt.year = y;
		dt.month = m;
		dt.day_of_month = d;
	}
	else
	{
		input.setstate( std::ios_base::failbit );
	}

	return input;
}

//--------------------------------------------------------------------------------

inline std::ostream& operator<<( std::ostream& input, const date& d )
{
	return input << d.year << '-' << d.month << '-' << d.day_of_month;
}

//--------------------------------------------------------------------------------

template < class Iterator, class Accessor, class Value = typename Iterator::value_type >
void counting_sort( const Iterator& begin_it, const Iterator& end_it, const Accessor& accessor, const Value& minimum, size_t slot_count )
{
	typedef typename std::iterator_traits< Iterator >::reference ref_type;
	typedef std::vector< size_t > slot_list;

	slot_list slots( slot_count ); // vector with slot_count slots initialized to 0

	std::accumulate( begin_it, end_it, &slots, [minimum, &accessor]( slot_list* ax, const ref_type value ){  
		size_t idx = static_cast< size_t >( accessor( value ) - minimum );
		++ax->at( idx );
		return ax;
	});

	std::partial_sum( slots.rbegin(), slots.rend(), slots.rbegin() );

	// as a result for all k slots[k] contains amount of items >= minimum + k

	typedef std::vector< typename std::iterator_traits< Iterator >::value_type > value_list;
	value_list sorted( std::distance( begin_it, end_it ) );

	std::for_each( begin_it, end_it, [&sorted, &slots, accessor, minimum]( const ref_type val ){
		size_t slot_idx = static_cast< size_t >( accessor( val ) - minimum );
		--slots.at( slot_idx );
		*std::next( sorted.rbegin(), slots.at( slot_idx ) ) = val;
	} );

	std::copy( begin( sorted ), end( sorted ), begin_it );
}

//--------------------------------------------------------------------------------

template < class Iterator >
void sort_dates( const Iterator& begin, const Iterator& end )
{
	if ( begin == end ) // there is nothing illegal in attempting to sort an empty range
		return;

	counting_sort( begin, end, []( const date& dt ){ return dt.day_of_month; }, 1, 31u );
	counting_sort( begin, end, []( const date& dt ){ return dt.month; }, 1, 12u );

	auto range = std::minmax_element( begin, end, []( const date& dt1, const date& dt2 ){
		return dt1.year < dt2.year;
	} );

	size_t range_size = static_cast< size_t >( range.second->year - range.first->year + 1 );

	counting_sort( begin, end, []( const date& dt ){ return dt.year; }, range.first->year, range_size );
}

//--------------------------------------------------------------------------------

int my_sqrt( int x )
{
	PRECONDITION( argument_non_negative, x >= 0 )

	int result = static_cast< int >( std::sqrt( x ) );

	POSTCONDITION( sqrt_lower_boundary, result * result <= x );
	POSTCONDITION( sqrt_upper_boundary, x < (result+1) * result );

	return result;
}

//--------------------------------------------------------------------------------


int main()
{
	typedef std::vector< date > date_list;

//	date_list dates = { 
//				{2017, 8, 17}, 
//				{2017, 6, 17}, 
//				{2017, 8, 15}, 
//				{2016, 4, 13}, 
//				{2016, 5, 13}, 
//				{2016, 4, 11}, 
//				{2016, 4, 12}, 
//				{2015, 2, 20}, 
//				{2014, 8, 18} 
//				};


	date_list dates;

	dates.assign( std::istream_iterator< date >( std::cin ), std::istream_iterator< date >() );

	sort_dates( std::begin( dates ), std::end( dates ) );

	std::copy( std::begin( dates ), std::end( dates ), std::ostream_iterator< date >( std::cout, "\n" ) );

	return 0;
}

