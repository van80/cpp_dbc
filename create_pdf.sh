#!/usr/bin/sh

script_dir=${0%/*}
build_dir="$script_dir/build"

function build_pdf {
	while read line
	do
	echo "$line"
	pdflatex -output-directory "$build_dir" "$line"
	done
}

find 'doc_src' -iname '*.tex' | build_pdf

#pdflatex -output-directory "$build_dir"
